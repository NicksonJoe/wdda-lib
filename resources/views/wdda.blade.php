<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Component Test</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.6.16/css/uikit.min.css"/>
</head>
<body>

<div class="uk-container uk-margin-top">
    <div>
        {!! formRadio('formRadio')->label('New radio button1')->attributes(['att1', 'attribute2' => 'value', 'att3']) !!}
        {!! formRadio('formRadio')->label('New radio button1') !!}
        {!! formRadio('formRadio')->label('New radio button1') !!}
        {!! formSelect('Select')->label('Select') !!}
        {!! formCheckbox('Checkbox')->label('Checkbox') !!}
        {!! formInput('Input4')->label('Input') !!}
        {!! formTextarea('Textarea')->label('Textarea') !!}
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.6.16/js/uikit.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.6.16/js/uikit-icons.min.js"></script>
</body>
</html>
