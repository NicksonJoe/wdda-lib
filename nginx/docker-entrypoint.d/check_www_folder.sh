#!/bin/bash

# Проверка наличия папки /var/www в контейнере wdda_nginx
docker exec wdda_nginx test -d /var/www && echo "Папка /var/www существует" || echo "Папка /var/www отсутствует"
