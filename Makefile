UID=$(shell id -u)
GID=$(shell id -g)

# Запуск всех контейнеров
up:
	UID=$(UID) GID=$(GID) docker-compose up -d --remove-orphans

# Билд всех контейнеров
build:
	UID=$(UID) GID=$(GID) docker-compose up -d --build --remove-orphans

# Остановка всех контейнеров
down:
	docker-compose down

# Перезапуск всех контейнеров
restart:
	docker-compose restart

# Логи для определенного сервиса (например, wdda_app)
#logs:
#	docker-compose logs -f wdda_app

# Просмотр статуса всех контейнеров
ps:
	docker-compose ps

# Установка переменных окружения и запуск всех контейнеров
set-up:
	export UID=$(UID) && export GID=$(GID) && make up

set-build:
	export UID=$(UID) && export GID=$(GID) && make build

# Остановка и удаление всех контейнеров, сетей и томов
clean:
	docker-compose down --volumes --remove-orphans

# Помощь (список доступных команд)
help:
	@echo "Usage:"
	@echo "  make up       - Start all containers"
	@echo "  make down     - Stop all containers"
	@echo "  make restart  - Restart all containers"
	@echo "  make logs     - Show logs for a specific service (e.g., wdda_app)"
	@echo "  make ps       - List all running containers"
	@echo "  make setup    - Set up environment variables and start all containers"
	@echo "  make clean    - Stop and remove all containers, networks, and volumes"
	@echo "  make help     - Show this help message"

# По умолчанию выводим справку
.DEFAULT_GOAL := help
